from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import cross_val_predict
from sklearn.metrics import f1_score

from env import *

def run(train_biz_feats, train_L, test_biz_feats,
        model=OneVsRestClassifier(LogisticRegression()),
        submission_file=None,
        cv_evaluation = False,
        full_model = True
       ):
    if cv_evaluation:
        train_L_pred = cross_val_predict(model, train_biz_feats, train_L, cv=5, n_jobs=5)
        cv_scores = np.array([f1_score(a,b) for (a,b) in zip(train_L, train_L_pred)])
        print('cv score is: {}'.format(cv_scores.mean()))
    else:
        cv_scores = None
        train_L_pred = None
    
    if full_model:
        model.fit(train_biz_feats, train_L)
        test_L_pred = model.predict(test_biz_feats)
        if submission_file is not None:
            write_submission(test_L_pred, submission_file, photos_in_test_biz.keys())
    else:
        model = None
        test_L_pred = None
    
    return model, test_L_pred, cv_scores, train_L_pred

def find_top_photos(scores, c, k=5, reverse=False):
    if reverse:
        ids = [train_ids[i] for i in scores[:,c].argsort()[0:k]]
    else:
        ids = [train_ids[i] for i in scores[:,c].argsort()[:-(k+1):-1]]
    return ids

def show_top_photos(scores, k=5, reverse=False):
    fig, axes = plt.subplots(n_labels, k, figsize=(15,30))
    for c, axes1 in enumerate(axes):
        for ax, id in zip(axes1, find_top_photos(scores, c, k, reverse=reverse)):
            show_image(id, ax)
            ax.set_axis_off()