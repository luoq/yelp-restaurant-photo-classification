from env import *
feats = get_feats()
def train_test_split(feats):
    return feats[:len(train_ids)], feats[len(train_ids):]
train_feats, test_feats = train_test_split(feats)
train_id_to_index = dict((x,i) for i,x in enumerate(train_ids))
test_id_to_index = dict((x,i) for i,x in enumerate(test_ids))

def index_to_id(i):
    if i < len(train_ids):
        return train_ids[i], False
    else:
        return test_ids[i-len(train_ids)], True

def gen_aggregated_feature(aggs=[np.mean], feats=feats):
    train_feats, test_feats = train_test_split(feats)
    def g(x):
        return np.concatenate([f(x, axis=0) for f in aggs])
    train_biz_feats = []
    for photos in train_labels.photos:
        I = [train_id_to_index[x] for x in photos]
        train_biz_feats.append(g(train_feats[I, :]))
    train_biz_feats = np.vstack(train_biz_feats)
    
    test_biz_feats = []
    for photos in photos_in_test_biz.values():
        I = [test_id_to_index[x] for x in photos]
        test_biz_feats.append(g(test_feats[I, :]))
    test_biz_feats = np.vstack(test_biz_feats)
    
    return train_biz_feats, test_biz_feats