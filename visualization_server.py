from flask import Flask, render_template
app = Flask(__name__)

from env import *
from feature import *
import pandas as pd

@app.route('/data/business/<int:i>')
def business_data(i):
    photo_ids = photos_in_biz(i)
    feats = train_feats[[train_id_to_index[x] for x in photo_ids]]
    df = pd.DataFrame(feats.T, columns=photo_ids)
    return df.to_csv(index_label='feature')

@app.route('/business/<int:i>')
def business(i):
    return render_template("business.html", biz_id=i)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
